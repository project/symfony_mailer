<?php

namespace Drupal\symfony_mailer\Plugin\Mailer;

/**
 * Defines the mailer interface for update module.
 */
interface UpdateMailerInterface {

  /**
   * Sends an update notification message.
   *
   * @return bool
   *   Whether successful.
   */
  public function notify();

}
