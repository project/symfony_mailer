<?php

namespace Drupal\symfony_mailer\Plugin\Mailer;

use Drupal\simplenews\SubscriberInterface;

/**
 * Defines the mailer interface for simplenews module subscriber emails.
 */
interface SimplenewsSubscriberMailerInterface {

  /**
   * Sends an email to a subscriber.
   *
   * @param string $operation
   *   The operation: subscribe or validate.
   * @param \Drupal\simplenews\SubscriberInterface $subscriber
   *   The subscriber.
   *
   * @return bool
   *   Whether successful.
   */
  public function sendToSubscriber(string $operation, SubscriberInterface $subscriber);

}
