<?php

namespace Drupal\symfony_mailer\Plugin\Mailer;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\symfony_mailer\Attribute\Mailer;
use Drupal\symfony_mailer\EmailInterface;

/**
 * Defines the Mailer plug-in for user registration password module.
 */
#[Mailer(
  id: "user_registrationpassword",
  sub_defs: [
    "register_confirmation_with_pass" => new TranslatableMarkup("Welcome (no approval required, password is set)"),
  ],
)]
class UserRegistrationPasswordMailer extends UserMailer {

  /**
   * {@inheritdoc}
   */
  public function build(EmailInterface $email) {
    parent::build($email);
    $this->tokenOptions([
      'callback' => 'user_registrationpassword_mail_tokens',
      'clear' => TRUE,
    ]);
  }

}
