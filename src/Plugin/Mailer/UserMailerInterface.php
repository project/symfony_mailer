<?php

namespace Drupal\symfony_mailer\Plugin\Mailer;

use Drupal\symfony_mailer\Processor\MailerPluginInterface;
use Drupal\user\UserInterface;

/**
 * Defines the mailer interface for user module.
 */
interface UserMailerInterface extends MailerPluginInterface {

  /**
   * Sends a user notification message.
   *
   * @param string $op
   *   The operation being performed on the account.
   * @param \Drupal\user\UserInterface $user
   *   The user to notify.
   *
   * @return bool
   *   Whether successful.
   */
  public function notify(string $op, UserInterface $user);

}
