<?php

namespace Drupal\symfony_mailer\Plugin\Mailer;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\symfony_mailer\Attachment;
use Drupal\symfony_mailer\Attribute\Mailer;
use Drupal\symfony_mailer\EmailInterface;
use Drupal\symfony_mailer\Processor\MailerPluginBase;
use Drupal\symfony_mailer\Processor\TokenProcessorTrait;

/**
 * Defines the Mailer plug-in for verification emails.
 */
#[Mailer(
  id: "symfony_mailer",
  service: VerifyMailerInterface::class,
  sub_defs: ["verify" => new TranslatableMarkup("Verification email")],
  common_adjusters: ["email_subject", "email_body"]
)]
class VerifyMailer extends MailerPluginBase implements VerifyMailerInterface {

  use TokenProcessorTrait;

  /**
   * {@inheritdoc}
   */
  public function verify($to) {
    return $this->newEmail('verify')
      ->setTo($to)
      ->send();
  }

  /**
   * {@inheritdoc}
   */
  public function build(EmailInterface $email) {
    $logo = \Drupal::service('extension.list.module')->getPath('symfony_mailer') . '/logo.png';
    $logo_uri = \Drupal::service('file_url_generator')->generateString($logo);

    // - Add a custom CSS library, defined in symfony_mailer.libraries.yml.
    // - The CSS is defined in verify.email.css.
    // - Set variables, used by the mailer policy defined in
    //   mailer_policy.mailer_policy.symfony_mailer.verify.yml.
    // - Add an attachment.
    $email->addLibrary('symfony_mailer/verify')
      ->attach(Attachment::fromPath($logo_uri, isUri: TRUE))
      ->setVariable('logo_url', $logo_uri)
      ->setVariable('day', date("l"));
  }

}
