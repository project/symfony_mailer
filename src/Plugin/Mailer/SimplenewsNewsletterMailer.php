<?php

namespace Drupal\symfony_mailer\Plugin\Mailer;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\simplenews\SubscriberInterface;
use Drupal\symfony_mailer\Address;
use Drupal\symfony_mailer\Attribute\Mailer;
use Drupal\symfony_mailer\EmailInterface;
use Drupal\symfony_mailer\Processor\MailerPluginBase;
use Drupal\symfony_mailer\Processor\TokenProcessorTrait;

/**
 * Defines the Mailer plug-in for simplenews module newsletter emails.
 *
 * Replaces \Drupal\simplenews\Mail\*
 */
#[Mailer(
  id: "simplenews.newsletter",
  service: SimplenewsNewsletterMailerInterface::class,
  sub_defs: [
    "node" => new TranslatableMarkup("Issue"),
  ],
  metadata_key: "simplenews_newsletter",
  common_adjusters: ["email_subject", "email_from"],
)]
class SimplenewsNewsletterMailer extends MailerPluginBase implements SimplenewsNewsletterMailerInterface {

  use TokenProcessorTrait;

  /**
   * {@inheritdoc}
   */
  public function sendIssue(ContentEntityInterface $issue, SubscriberInterface $subscriber, string $mode) {
    $address = new Address($subscriber->getMail(), NULL, $subscriber->getLangcode(), $subscriber->getUser());
    return $this->newEmail('node')
      ->setEntityParam($issue->simplenews_issue->entity)
      // Token replacement requires the non-standard key (!= entity type).
      ->setParam('newsletter', $issue->simplenews_issue->entity)
      ->setParam('issue', $issue)
      ->setEntityParam($subscriber)
      ->setEntityParam($issue)
      ->setVariable('mode', $mode)
      ->setTo($address)
      ->send();
  }

  /**
   * {@inheritdoc}
   */
  public function build(EmailInterface $email) {
    parent::build($email);
    $newsletter = $email->getParam('simplenews_newsletter');
    $mode = $email->getVariables()['mode'];
    $temp_subscriber = $mode && !$email->getParam('simplenews_subscriber')->id();
    // Match existing view mode from simplenews module.
    $email->setEntityVariable('issue', 'email_html')
      ->addTextHeader('Precedence', 'bulk')
      ->setVariable('opt_out_hidden', !$newsletter->isAccessible() || $temp_subscriber)
      ->setVariable('reason', $newsletter->reason ?? '')
      // @deprecated
      ->setVariable('test', $mode == 'test');

    // @todo Create SubscriberInterface::getUnsubscribeUrl().
    if ($unsubscribe_url = \Drupal::token()->replace('[simplenews-subscriber:unsubscribe-url]', $email->getParams(), ['clear' => TRUE])) {
      $email->addTextHeader('List-Unsubscribe', "<$unsubscribe_url>");
    }
  }

}
