<?php

namespace Drupal\symfony_mailer\Plugin\Mailer;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderType;
use Drupal\symfony_mailer\Attribute\Mailer;
use Drupal\symfony_mailer\EmailInterface;
use Drupal\symfony_mailer\Processor\MailerPluginBase;

/**
 * Defines the Mailer plug-in for commerce order module.
 *
 * The template variable 'body' is generated from the order type settings for
 * "Manage Display" (1). The "Order item table" formatter is generated from the
 * "Order items" view (2).
 * (1) /admin/commerce/config/order-types/XXX/edit/display/email
 * (2) /admin/structure/views/view/commerce_order_item_table.
 *
 * Replaces:
 * - \Drupal\commerce_order\Mail\OrderReceiptMail
 * - \Drupal\commerce\MailHandler
 */
#[Mailer(
  id: "commerce_order",
  service: CommerceOrderMailerInterface::class,
  label: new TranslatableMarkup("Commerce order"),
  sub_defs: [
    "receipt" => new TranslatableMarkup("Receipt"),
    "resend_receipt" => new TranslatableMarkup("Resend receipt"),
  ],
  metadata_key: "commerce_order_type",
  common_adjusters: ["email_subject", "email_body", "email_bcc"],
)]
class CommerceOrderMailer extends MailerPluginBase implements CommerceOrderMailerInterface {

  /**
   * {@inheritdoc}
   */
  public function sendReceipt(OrderInterface $order, bool $resend = FALSE) {
    $sub_type = $resend ? 'resend_receipt' : 'receipt';
    $customer = $order->getCustomer();
    $to = $customer->isAuthenticated() ? $customer : $order->getEmail();
    return $this->newEmail($sub_type)
      ->setEntityParam(OrderType::load($order->bundle()))
      ->setEntityParam($order)
      ->setTo($to)
      ->send();
  }

  /**
   * {@inheritdoc}
   */
  public function build(EmailInterface $email) {
    $order = $email->getParam('commerce_order');
    $store = $order->getStore();

    $email->setEntityVariable('commerce_order')
      ->addLibrary('symfony_mailer/commerce_order')
      ->setVariable('order_number', $order->getOrderNumber())
      ->setVariable('store', $store->getName());

    // Get the actual email value without picking up the default from the site
    // mail. Instead we prefer to default from Mailer policy.
    if ($store_email = $store->get('mail')->value) {
      $email->setFrom($store_email);
    }
  }

}
