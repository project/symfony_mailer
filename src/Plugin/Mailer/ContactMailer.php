<?php

namespace Drupal\symfony_mailer\Plugin\Mailer;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\contact\MessageInterface;
use Drupal\symfony_mailer\Address;
use Drupal\symfony_mailer\Attribute\Mailer;
use Drupal\symfony_mailer\EmailInterface;
use Drupal\symfony_mailer\Processor\MailerPluginBase;
use Drupal\user\Entity\User;

/**
 * Defines the Mailer plug-in for contact module page forms.
 *
 * Replaces \Drupal\contact\MailHandler.
 */
#[Mailer(
  id: "contact",
  service: ContactMailerInterface::class,
  common_adjusters: ["email_subject", "email_body"],
  sub_defs: [
    "page" => [
      "label" => new TranslatableMarkup("Site"),
      "metadata_key" => "contact_form",
      "sub_defs" => [
        "mail" => [
          "label" => new TranslatableMarkup("Message"),
          "common_adjusters" => ["email_subject", "email_body", "email_to"],
        ],
        "copy" => new TranslatableMarkup("Sender copy"),
        "autoreply" => new TranslatableMarkup("Auto-reply"),
      ],
    ],
    "user" => [
      "label" => new TranslatableMarkup("Personal"),
      "sub_defs" => [
        "mail" => new TranslatableMarkup("Message"),
        "copy" => new TranslatableMarkup("Sender copy"),
      ],
    ],
  ],
)]
class ContactMailer extends MailerPluginBase implements ContactMailerInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function sendMailMessages(MessageInterface $message) {
    $success = $this->sendEmail('mail', $message);
    if ($success && $message->copySender()) {
      $success = $this->sendEmail('copy', $message);
    }
    if ($success && !$message->isPersonal()) {
      $success = $this->sendEmail('autoreply', $message);
    }
    return $success;
  }

  /**
   * {@inheritdoc}
   */
  public function build(EmailInterface $email) {
    /** @var \Drupal\symfony_mailer\Address $sender */
    $sender = $email->getParam('sender');
    $message = $email->getParam('contact_message');
    $sender_name = $sender->getDisplayName();

    if ($account = $sender->getAccount()) {
      $sender_url = User::load($account->id())->toUrl('canonical')->toString();
    }
    else {
      // Clarify that the sender name is not verified; it could potentially
      // clash with a username on this site.
      $sender_name = $this->t('@name (not verified)', ['@name' => $sender_name]);
      $sender_url = Url::fromUri('mailto:' . $sender->getEmail());
    }

    $email->setEntityVariable('contact_message')
      ->addLibrary('symfony_mailer/contact')
      ->setVariable('subject', $message->getSubject())
      ->setVariable('site_name', \Drupal::config('system.site')->get('name'))
      ->setVariable('sender_name', $sender_name)
      ->setVariable('sender_url', $sender_url);

    if ($message->isPersonal()) {
      $recipient = $email->getParam('recipient');
      $email->setVariable('recipient_name', $recipient->getDisplayName())
        ->setVariable('recipient_edit_url', $recipient->toUrl('edit-form')->toString());
    }
    else {
      $email->setVariable('form', $email->getParam('contact_form')->label())
        ->setVariable('form_url', Url::fromRoute('<current>')->toString());
    }

    if ($email->getTag(2) == 'mail') {
      $email->setReplyTo($sender);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function sendEmail(string $tag3, MessageInterface $message) {
    $personal = $message->isPersonal();
    $tag2 = $personal ? 'user' : 'page';
    $sender = Address::fromCurrent($message->getSenderMail(), $message->getSenderName());
    $email = $this->newEmail("$tag2.$tag3")
      ->setEntityParam($message)
      ->setParam('sender', $sender);

    if ($personal) {
      $recipient = $message->getPersonalRecipient();
      $email->setParam('recipient', $recipient);
      if ($tag3 == 'mail') {
        $email->setTo($email->getParam('recipient'));
      }
    }
    else {
      $email->setEntityParam($message->getContactForm());
    }

    if ($tag3 != 'mail') {
      $email->setTo($sender);
    }

    return $email->send();
  }

}
