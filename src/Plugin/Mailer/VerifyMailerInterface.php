<?php

namespace Drupal\symfony_mailer\Plugin\Mailer;

/**
 * Defines the mailer interface for verification emails.
 */
interface VerifyMailerInterface {

  /**
   * Sends a verification email.
   *
   * @param mixed $to
   *   The to addresses, see Address::convert().
   *
   * @return bool
   *   Whether successful.
   */
  public function verify($to);

}
