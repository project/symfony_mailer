<?php

namespace Drupal\symfony_mailer\Plugin\Mailer;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\simplenews\SubscriberInterface;
use Drupal\symfony_mailer\Address;
use Drupal\symfony_mailer\Attribute\Mailer;
use Drupal\symfony_mailer\Processor\MailerPluginBase;
use Drupal\symfony_mailer\Processor\TokenProcessorTrait;

/**
 * Defines the Mailer plug-in for simplenews module subscriber emails.
 *
 * Replaces parts of:
 * - \Drupal\simplenews\Mail\MailBuilder
 * - \Drupal\simplenews\Mail\Mailer.
 */
#[Mailer(
  id: "simplenews.subscriber",
  service: SimplenewsSubscriberMailerInterface::class,
  label: new TranslatableMarkup("Simplenews subscriber"),
  sub_defs: [
    "subscribe" => new TranslatableMarkup("Confirmation"),
    "validate" => new TranslatableMarkup("Validate"),
  ],
  common_adjusters: ["email_subject", "email_body"],
)]
class SimplenewsSubscriberMailer extends MailerPluginBase implements SimplenewsSubscriberMailerInterface {

  use TokenProcessorTrait;

  /**
   * {@inheritdoc}
   */
  public function sendToSubscriber(string $operation, SubscriberInterface $subscriber) {
    $address = new Address($subscriber->getMail(), NULL, $subscriber->getLangcode(), $subscriber->getUser());
    return $this->newEmail($operation)
      ->setEntityParam($subscriber)
      ->setTo($address)
      ->send();
  }

}
