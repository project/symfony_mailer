<?php

namespace Drupal\symfony_mailer\Plugin\Mailer;

use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\symfony_mailer\Attribute\Mailer;
use Drupal\symfony_mailer\EmailInterface;
use Drupal\symfony_mailer\Exception\SkipMailException;
use Drupal\symfony_mailer\Processor\MailerPluginBase;
use Drupal\update\UpdateManagerInterface;

/**
 * Defines the Mailer plug-in for update module.
 *
 * Replaces _update_cron_notify().
 *
 * The notification address is configured using Mailer Policy for
 * UpdateMailer. Set a dummy value in update.settings to force the update
 * module to send an email. NB UpdateMailer ignores the passed 'To'
 * address so the dummy value will never be used.
 */
#[Mailer(
  id: "update",
  service: UpdateMailerInterface::class,
  sub_defs: ["status_notify" => new TranslatableMarkup("Available updates")],
  common_adjusters: ["email_subject", "email_body", "email_to"],
)]
class UpdateMailer extends MailerPluginBase implements UpdateMailerInterface {

  /**
   * {@inheritdoc}
   */
  public function notify() {
    return $this->newEmail('status_notify')->send();
  }

  /**
   * {@inheritdoc}
   */
  public function build(EmailInterface $email) {
    if (empty($email->getTo())) {
      throw new SkipMailException('No update notification address configured.');
    }

    $notify_all = (\Drupal::config('update.settings')->get('notification.threshold') == 'all');
    \Drupal::moduleHandler()->loadInclude('update', 'install');
    $requirements = update_requirements('runtime');

    $messages = [];
    foreach (['core', 'contrib'] as $report_type) {
      $status = $requirements["update_$report_type"];
      if (isset($status['severity'])) {
        if ($status['severity'] == REQUIREMENT_ERROR || ($notify_all && $status['reason'] == UpdateManagerInterface::NOT_CURRENT)) {
          $messages[] = _update_message_text($report_type, $status['reason']);
        }
      }
    }

    $site_name = \Drupal::config('system.site')->get('name');
    $email->setVariable('site_name', $site_name)
      ->setVariable('update_status', Url::fromRoute('update.status')->toString())
      ->setVariable('update_settings', Url::fromRoute('update.settings')->toString())
      ->setVariable('messages', $messages);

    if (Settings::get('allow_authorize_operations', TRUE)) {
      $email->setVariable('update_manager', Url::fromRoute('update.report_update')->toString());
    }
  }

}
