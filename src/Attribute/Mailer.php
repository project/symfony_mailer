<?php

namespace Drupal\symfony_mailer\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Component\Render\MarkupInterface;

/**
 * The Mailer attribute.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class Mailer extends Plugin {

  /**
   * Constructor for Mailer attribute.
   *
   * @param string $id
   *   The plugin ID. This is a tag prefix that indicates what emails this
   *   plugin builds. All emails that are built have a tag that starts with
   *   this prefix.
   * @param string $service
   *   The interface class registered as a mailer service.
   * @param Drupal\Component\Render\MarkupInterface $label
   *   The human-readable name of the plugin. Leave blank to derive from an
   *   entity type or module matching the ID.
   * @param array $sub_defs
   *   Array of sub-types. The array key is the sub-type value and the value is
   *   either the human-readable label, or another array of the same form.
   * @param ?string $metadata_key
   *   Email parameters can be used as metadata to group emails and configure
   *   them separately. For example the body of the contact message auto-reply
   *   can be different for each contact form. The value is a parameter key
   *   whose value is a config entity.
   * @param string[] $common_adjusters
   *   Array of common adjuster IDs.
   */
  public function __construct(
    public readonly string $id,
    public readonly ?string $service = NULL,
    public readonly ?MarkupInterface $label = NULL,
    public readonly array $sub_defs = [],
    public readonly ?string $metadata_key = NULL,
    public readonly array $common_adjusters = [],
  ) {}

}
