<?php

namespace Drupal\symfony_mailer;

use Drupal\Core\Session\AccountInterface;

/**
 * Defines an extended Email interface that adds internal functions.
 *
 * @internal
 */
interface InternalEmailInterface extends EmailInterface {

  /**
   * Runs processing of the current phase for all email processors.
   *
   * @return $this
   */
  public function process();

  /**
   * Customizes the email.
   *
   * Valid: initialisation.
   *
   * @param string $langcode
   *   The language code.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account.
   *
   * @return $this
   */
  public function customize(string $langcode, AccountInterface $account);

  /**
   * Renders the email.
   *
   * Valid: build.
   *
   * @return $this
   */
  public function render();

  /**
   * Gets an array of 'suggestions'.
   *
   * The suggestions are used to select mailers, templates and policy
   * configuration in based on the email tag.
   *
   * @param string $initial
   *   The initial suggestion.
   * @param string $join
   *   The 'glue' to join each suggestion part with.
   *
   * @return array
   *   Suggestions, formed by taking the initial value and incrementally adding
   *   the parts of the tag breaking at each dot.
   */
  public function getSuggestions(string $initial, string $join);

  /**
   * Gets the inner Symfony email to send.
   *
   * Valid: after rendering.
   *
   * @return \Symfony\Component\Mime\Email
   *   Inner Symfony email.
   */
  public function getSymfonyEmail();

  /**
   * Sets the error message from sending the email.
   *
   * Valid: after sending.
   *
   * @param string $error
   *   The error message.
   *
   * @return $this
   */
  public function setError(string $error);

}
