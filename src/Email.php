<?php

namespace Drupal\symfony_mailer;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Component\Render\PlainTextOutput;
use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\symfony_mailer\Processor\CallbackEmailProcessor;
use Drupal\symfony_mailer\Processor\EmailProcessorInterface;
use Drupal\user\Entity\User;
use Symfony\Component\Mime\Email as SymfonyEmail;

/**
 * Defines the email class.
 */
class Email extends BaseEmail implements InternalEmailInterface {

  /**
   * The body array.
   *
   * @var array
   */
  protected $body = [];

  /**
   * The processors.
   *
   * @var array
   */
  protected $processors = [];

  /**
   * The processors remaining to process in the current phase.
   *
   * @var array
   */
  protected $processorQueue = [];

  /**
   * Set to TRUE to re-sort the processor queue.
   *
   * @var bool
   */
  protected $processorSort;

  /**
   * The language code.
   *
   * @var string
   */
  protected $langcode;

  /**
   * The params.
   *
   * @var string[]
   */
  protected $params = [];

  /**
   * The variables.
   *
   * @var string[]
   */
  protected $variables = [];

  /**
   * The account for the recipient (can be anonymous).
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The theme.
   *
   * @var string
   */
  protected $theme = '';

  /**
   * The libraries.
   *
   * @var array
   */
  protected $libraries = [];

  /**
   * The error message from sending.
   *
   * @var string
   */
  protected $errorMessage;

  /**
   * Constructs the Email object.
   *
   * @param \Drupal\symfony_mailer\MailerInterface $mailer
   *   Mailer service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $themeManager
   *   The theme manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   * @param string $tag
   *   Tag used to identify the type or source of this email.
   *   @see self::getTag()
   *
   * @internal
   */
  public function __construct(
    protected readonly MailerInterface $mailer,
    protected readonly RendererInterface $renderer,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    protected readonly ThemeManagerInterface $themeManager,
    protected readonly ConfigFactoryInterface $configFactory,
    protected readonly string $tag,
  ) {
    $this->inner = new SymfonyEmail();
  }

  /**
   * {@inheritdoc}
   */
  public function getPhase() {
    return $this->phase;
  }

  /**
   * {@inheritdoc}
   */
  public function addProcessor(EmailProcessorInterface $processor) {
    $this->valid(self::PHASE_INIT);
    $this->processors[$processor->getId()] = $processor;
    $this->processorQueue[] = $processor;
    $this->processorSort = TRUE;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addCallback(callable $function, int $phase = self::PHASE_BUILD, int $weight = self::DEFAULT_WEIGHT, ?string $id = NULL) {
    $this->addProcessor((new CallbackEmailProcessor($weight, $id))->setCallback($function, $phase));
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function removeProcessor(string $id) {
    $this->valid(self::PHASE_INIT);
    unset($this->processors[$id]);
    $this->processorQueue = array_filter($this->processorQueue, fn(EmailProcessorInterface $processor) => ($processor->id() != $id));
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessors() {
    return $this->processors;
  }

  /**
   * {@inheritdoc}
   */
  public function getLangcode() {
    return $this->langcode;
  }

  /**
   * {@inheritdoc}
   */
  public function setParams(array $params = []) {
    $this->valid(min_phase: self::PHASE_INIT);
    $this->params = $params;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setParam(string $key, $value) {
    $this->valid(min_phase: self::PHASE_INIT);
    $this->params[$key] = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setEntityParam(EntityInterface $entity) {
    return $this->setParam($entity->getEntityTypeId(), $entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getParams() {
    return $this->params;
  }

  /**
   * {@inheritdoc}
   */
  public function getParam(string $key) {
    return $this->params[$key] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function send() {
    $this->valid(self::PHASE_INIT);
    return $this->mailer->send($this);
  }

  /**
   * {@inheritdoc}
   */
  public function getAccount() {
    return $this->account;
  }

  /**
   * {@inheritdoc}
   */
  public function setBody($body) {
    $this->valid(self::PHASE_BUILD);
    $this->body = $body;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBody() {
    $this->valid(self::PHASE_BUILD);
    return $this->body;
  }

  /**
   * {@inheritdoc}
   */
  public function setVariables(array $variables) {
    $this->valid(self::PHASE_BUILD, self::PHASE_INIT);
    $this->variables = $variables;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setVariable(string $key, $value) {
    $this->valid(self::PHASE_BUILD, self::PHASE_INIT);
    $this->variables[$key] = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setEntityVariable(string $key, $view_mode = 'email') {
    $this->valid(self::PHASE_BUILD);
    $entity = $this->getParam($key);
    $build = $this->entityTypeManager->getViewBuilder($entity->getEntityTypeId())
      ->view($entity, $view_mode);
    $this->setVariable($key, $build);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getVariables() {
    return $this->variables;
  }

  /**
   * {@inheritdoc}
   */
  public function getTag(?int $part = NULL) {
    if (is_null($part)) {
      return $this->tag;
    }
    return explode('.', $this->tag)[$part] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getSuggestions(string $initial, string $join) {
    $part_array = explode('.', $this->tag);
    $part = $initial ?: array_shift($part_array);
    $suggestions[] = $part;

    while ($part_array) {
      $part .= $join . array_shift($part_array);
      $suggestions[] = $part;
    }

    return $suggestions;
  }

  /**
   * {@inheritdoc}
   */
  public function setTheme(string $theme_name) {
    $this->valid(self::PHASE_INIT);
    $this->theme = $theme_name;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTheme() {
    if (!$this->theme) {
      $this->theme = $this->themeManager->getActiveTheme()->getName();
    }
    return $this->theme;
  }

  /**
   * {@inheritdoc}
   */
  public function addLibrary(string $library) {
    $this->valid(self::PHASE_BUILD);
    $this->libraries[] = $library;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries() {
    return $this->libraries;
  }

  /**
   * {@inheritdoc}
   */
  public function setTransport(string $transport) {
    $this->valid();
    $this->getHeaders()->addHeader('X-Transport', $transport);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTransport() {
    return $this->getHeaders()->getHeaderBody('X-Transport');
  }

  /**
   * {@inheritdoc}
   */
  public function getError() {
    return $this->errorMessage;
  }

  /**
   * {@inheritdoc}
   */
  public function process() {
    $this->processorQueue = $this->getProcessors();
    $this->processorSort = TRUE;

    // While processing PHASE_INIT, each processor may add or remove others.
    // Therefore we use a queue and a while loop rather than a for loop.
    while ($this->processorQueue) {
      if ($this->processorSort) {
        usort($this->processorQueue, function ($a, $b) {
          return $a->getWeight($this->phase) <=> $b->getWeight($this->phase);
        });
        $this->processorSort = FALSE;
      }

      $processor = array_shift($this->processorQueue);

      switch ($this->phase) {
        case self::PHASE_INIT:
          $processor->init($this);
          break;

        case self::PHASE_BUILD:
          $processor->build($this);
          break;

        case self::PHASE_POST_RENDER:
          $processor->postRender($this);
          break;

        case self::PHASE_POST_SEND:
          $processor->postSend($this);
          break;
      }
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function customize(string $langcode, AccountInterface $account) {
    $this->valid(self::PHASE_INIT);
    $this->langcode = $langcode;
    $this->account = $account;
    $this->phase = self::PHASE_BUILD;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $this->valid(self::PHASE_BUILD);

    // Render subject.
    if ($this->subjectReplace && $this->variables) {
      $subject = [
        '#type' => 'inline_template',
        '#template' => $this->subject,
        '#context' => $this->variables,
      ];
      $this->subject = $this->renderer->renderInIsolation($subject);
    }

    if ($this->subject instanceof MarkupInterface) {
      $this->subject = PlainTextOutput::renderFromHtml($this->subject);
    }

    // Render body.
    $body = ['#theme' => 'email', '#email' => $this];
    $html = $this->renderer->renderInIsolation($body);
    $this->phase = self::PHASE_POST_RENDER;
    $this->setHtmlBody($html);
    $this->body = [];

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSymfonyEmail() {
    $this->valid(min_phase: self::PHASE_POST_RENDER);
    $this->phase = self::PHASE_POST_SEND;

    if ($this->subject) {
      $this->inner->subject($this->subject);
    }

    // Addresses.
    $this->inner->sender($this->sender->getSymfony());
    $headers = $this->getHeaders();
    foreach ($this->addresses as $name => $addresses) {
      $value = [];
      foreach ($addresses as $address) {
        $value[] = $address->getSymfony();
      }
      if ($value) {
        // Convert headers to camel case.
        $headers->addMailboxListHeader(ucwords($name, '-'), $value);
      }
    }

    // Attachments.
    foreach ($this->attachments as $attachment) {
      if ($attachment->hasAccess()) {
        $this->inner->addPart($attachment);
        if (($attachment->getMediaType() == 'image') && ($attachment->getUri() != NULL)) {
          $replace_uri = TRUE;
        }
      }
    }

    if (isset($replace_uri) && $body = $this->getHtmlBody()) {
      $dom = Html::load($body);

      foreach ($dom->getElementsByTagName('img') as $img) {
        $uri = $img->getAttribute('src');

        if ($attach = $this->attachments[$uri] ?? NULL) {
          $img->setAttribute('src', 'cid:' . $attach->getContentId());
        }
      }

      $body = Html::serialize($dom);
      $this->phase = self::PHASE_POST_RENDER;
      $this->setHtmlBody($body);
      $this->phase = self::PHASE_POST_SEND;
    }

    return $this->inner;
  }

  /**
   * {@inheritdoc}
   */
  public function setError(string $error) {
    $this->valid(self::PHASE_POST_SEND, self::PHASE_POST_SEND);
    $this->errorMessage = $error;
    return $this;
  }

  /**
   * {@inheritdoc}
   *
   * Serialization is intended only for testing.
   *
   * @internal
   */
  public function __serialize() {
    // Exclude $this->params, $this->variables as they may not serialize.
    return [$this->tag, $this->phase, $this->subject, $this->langcode,
      $this->account ? $this->account->id() : '', $this->theme,
      $this->libraries, $this->inner,
      $this->addresses, $this->sender,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function __unserialize(array $data) {
    [$this->tag, $this->phase, $this->subject,
      $this->langcode, $account_id, $this->theme,
      $this->libraries, $this->inner,
      $this->addresses, $this->sender,
    ] = $data;

    if ($account_id) {
      $this->account = User::load($account_id);
    }
  }

}
