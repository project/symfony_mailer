<?php

namespace Drupal\symfony_mailer;

use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\AutowiringFailedException;

/**
 * Defines a trait for automatically wiring dependencies from the container.
 *
 * Extension of \Drupal\Core\DependencyInjection\AutowireTrait.
 * - Add caching to reduce performance issues.
 * - Allow passing extra arguments for the constructor.
 * - Allow optional dependencies.
 */
trait AutowireTrait {

  /**
   * Array of service names to use when calling the constructor.
   *
   * @var array
   */
  protected static $services = [];

  /**
   * Instantiates a new instance of the implementing class using autowiring.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container this instance should use.
   * @param mixed ...$args
   *   Extra arguments for the constructor.
   *
   * @return static
   */
  public static function create(ContainerInterface $container, ...$args) {
    if (!static::$services) {
      $constructor = new \ReflectionMethod(static::class, '__construct');
      $parameters = array_slice($constructor->getParameters(), count($args));
      foreach ($parameters as $parameter) {
        $service = ltrim((string) $parameter->getType(), '?');
        foreach ($parameter->getAttributes(Autowire::class) as $attribute) {
          $service = (string) $attribute->newInstance()->value;
        }

        if (!$container->has($service)) {
          if (!$parameter->allowsNull()) {
            throw new AutowiringFailedException($service, sprintf('Cannot autowire service "%s": argument "$%s" of method "%s::_construct()", you should configure its value explicitly.', $service, $parameter->getName(), static::class));
          }
          static::$services[] = NULL;
        }
        else {
          static::$services[] = $service;
        }
      }
    }

    foreach (static::$services as $service) {
      $args[] = $service ? $container->get($service) : NULL;
    }
    return new static(...$args);
  }

}
