<?php

namespace Drupal\symfony_mailer\Processor\automatic;

use Drupal\Core\Access\AccessResult;
use Drupal\symfony_mailer\EmailInterface;
use Drupal\symfony_mailer\Processor\EmailProcessorInterface;
use Drupal\symfony_mailer\Processor\EmailProcessorTrait;

/**
 * Defines the Attachment access Email Processor.
 */
class AttachmentAccessEmailProcessor implements EmailProcessorInterface {

  use EmailProcessorTrait;

  /**
   * The allowed schemes for the attachment URI.
   *
   * The value is a boolean:
   * - TRUE: check the URI can be opened.
   * - FALSE: no checking required.
   *
   * @var array
   */
  protected const ALLOWED_SCHEMES = [
    'http' => TRUE,
    'https' => TRUE,
    'public' => FALSE,
    '_data_' => FALSE,
  ];

  /**
   * {@inheritdoc}
   */
  public function postRender(EmailInterface $email) {
    foreach ($email->getAttachments() as $attachment) {
      $uri = $attachment->getUri();
      $scheme = $uri ? parse_url($uri, PHP_URL_SCHEME) : '_data_';
      $check = self::ALLOWED_SCHEMES[$scheme] ?? NULL;

      if (!is_null($check)) {
        if ($check) {
          $handle = @fopen($uri, 'r');
          if (!$handle) {
            continue;
          }
          fclose($handle);
        }
        $attachment->setAccess(AccessResult::allowed());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight(int $phase) {
    return 600;
  }

}
