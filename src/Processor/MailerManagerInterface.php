<?php

namespace Drupal\symfony_mailer\Processor;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Provides the interface for the mailer plugin manager.
 */
interface MailerManagerInterface extends PluginManagerInterface {

  /**
   * Gets a definition or sub-definition for the specified tag.
   *
   * @param string $tag
   *   The tag.
   *
   * @return array
   *   The definition.
   */
  public function getTagDefinition(string $tag);

}
