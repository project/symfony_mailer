<?php

namespace Drupal\symfony_mailer\Processor;

use Drupal\symfony_mailer\EmailInterface;

/**
 * Defines a trait to help writing EmailProcessorInterface implementations.
 */
trait EmailProcessorTrait {

  /**
   * {@inheritdoc}
   */
  public function init(EmailInterface $email) {
  }

  /**
   * {@inheritdoc}
   */
  public function build(EmailInterface $email) {
  }

  /**
   * {@inheritdoc}
   */
  public function postRender(EmailInterface $email) {
  }

  /**
   * {@inheritdoc}
   */
  public function postSend(EmailInterface $email) {
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight(int $phase) {
    return EmailInterface::DEFAULT_WEIGHT;
  }

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return static::class;
  }

}
