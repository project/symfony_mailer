<?php

namespace Drupal\symfony_mailer\Processor;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides the mailer plugin manager.
 */
class MailerManager extends DefaultPluginManager implements MailerManagerInterface {

  /**
   * Array of definitions and sub-definitions indexed by tag.
   *
   * @var array
   */
  protected $tagDefinitions = NULL;

  /**
   * Constructs the MailerManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleExtensionList $moduleList
   *   The module extension list.
   *
   * @internal
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    protected readonly ModuleExtensionList $moduleList,
  ) {
    parent::__construct('Plugin/Mailer', $namespaces, $module_handler, 'Drupal\symfony_mailer\SubMailerInterface', 'Drupal\symfony_mailer\Attribute\Mailer');
    $this->setCacheBackend($cache_backend, 'symfony_mailer_definitions');
    $this->alterInfo('symfony_mailer_info');
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    $definition = $this->getDefinition($plugin_id);
    if ($service = $definition['service']) {
      return \Drupal::service($service);
    }
    return parent::createInstance($plugin_id, $configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    // Look up the related entity or module, which can be used to generate the
    // label and provider.
    [$base_id] = explode('.', $plugin_id);
    if ($meta_key = $definition['metadata_key']) {
      if ($entity_def = $this->entityTypeManager->getDefinition($meta_key, FALSE)) {
        $definition['label'] ??= $entity_def->getLabel();
        $definition['provider'] = $entity_def->getProvider();
      }
    }
    elseif ($this->moduleHandler->moduleExists($base_id)) {
      $definition['label'] ??= $this->moduleList->getName($base_id);
    }

    // Normally, the provider is defaulted from the namespace, but we prefer
    // instead to set from the ID. This allows one module to proxy a
    // definition for another, and it will be ignored if the target module
    // isn't enabled.
    $definition['provider'] = $base_id;

    $definition['labels'] = [$definition['label']];
    $this->checkSubDef($definition);
  }

  /**
   * {@inheritdoc}
   */
  public function clearCachedDefinitions() {
    parent::clearCachedDefinitions();
    $this->tagDefinitions = NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getTagDefinition(string $tag) {
    if ($this->tagDefinitions == NULL) {
      $this->tagDefinitions = [];
      $todo = array_values($this->getDefinitions());
      while ($def = array_shift($todo)) {
        $this->tagDefinitions[$def['id']] = $def;
        $todo = array_merge($todo, array_values($def['sub_defs']));
      }
    }
    return $this->tagDefinitions[$tag] ?? NULL;
  }

  /**
   * Recursively checks definitions, adding defaults and normalising.
   *
   * @param mixed $definition
   *   Definition to check.
   */
  protected function checkSubDef(&$definition) {
    foreach ($definition['sub_defs'] as $id => &$sub_def) {
      if (!is_array($sub_def)) {
        $sub_def = ['label' => $sub_def];
      }
      $sub_def['id'] = $definition['id'] . ".$id";
      $sub_def['service'] ??= $definition['service'];
      $sub_def['sub_defs'] ??= [];
      $sub_def['metadata_key'] ??= $definition['metadata_key'];
      $sub_def['common_adjusters'] ??= $definition['common_adjusters'];
      $sub_def['labels'] = array_merge($definition['labels'], [$sub_def['label']]);
      $this->checkSubDef($sub_def);
    }
  }

}
