<?php

namespace Drupal\symfony_mailer\Processor;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\symfony_mailer\Attribute\Mailer;
use Drupal\symfony_mailer\EmailInterface;
use Drupal\symfony_mailer\MailerInterface;

/**
 * Defines the base class for Mailer plug-ins.
 */
abstract class MailerPluginBase implements MailerPluginInterface, PluginInspectionInterface {

  use EmailProcessorTrait;

  /**
   * {@inheritdoc}
   */
  const DEFAULT_WEIGHT = 300;

  /**
   * The plugin implementation definition.
   *
   * @var array
   */
  protected $pluginDefinition = NULL;

  /**
   * Email processors to add to the next email that is sent.
   *
   * @var \Drupal\symfony_mailer\Processor\EmailProcessorInterface[]
   */
  protected $processors = [];

  /**
   * {@inheritdoc}
   */
  public function addProcessor(EmailProcessorInterface $processor) {
    $this->processors[] = $processor;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addCallback(callable $function, int $phase = EmailInterface::PHASE_BUILD, int $weight = EmailInterface::DEFAULT_WEIGHT, ?string $id = NULL) {
    $this->addProcessor((new CallbackEmailProcessor($weight, $id))->setCallback($function, $phase));
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight(int $phase) {
    return static::DEFAULT_WEIGHT;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId() {
    return $this->getPluginDefinition()['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginDefinition() {
    if (!$this->pluginDefinition) {
      $this->pluginDefinition = (new \ReflectionClass($this))->getAttributes(Mailer::class)[0]->getArguments();
    }
    return $this->pluginDefinition;
  }

  /**
   * Creates a new email.
   *
   * @param string $sub_tag
   *   Sub-tag to add onto the plugin ID.
   *
   * @return \Drupal\symfony_mailer\EmailInterface
   *   The email.
   */
  protected function newEmail(string $sub_tag) {
    $mailer = \Drupal::service(MailerInterface::class);
    $email = $mailer->newEmail($this->getPluginId() . ".$sub_tag")->addProcessor($this);
    foreach ($this->processors as $processor) {
      $email->addProcessor($processor);
    }
    $this->processors = [];
    return $email;
  }

}
