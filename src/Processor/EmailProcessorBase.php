<?php

namespace Drupal\symfony_mailer\Processor;

use Drupal\Core\Plugin\PluginBase;

/**
 * Defines the base class for EmailProcessorInterface plug-ins.
 */
abstract class EmailProcessorBase extends PluginBase implements EmailProcessorInterface {

  use EmailProcessorTrait;

  /**
   * The default weight. This must be set by the sub-class.
   */
  const DEFAULT_WEIGHT = NULL;

  /**
   * {@inheritdoc}
   */
  public function getWeight(int $phase) {
    $weight = $this->getPluginDefinition()['weight'] ?? static::DEFAULT_WEIGHT;
    return is_array($weight) ? $weight[$phase] : $weight;
  }

}
