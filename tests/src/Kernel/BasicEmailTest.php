<?php

namespace Drupal\Tests\symfony_mailer\Kernel;

/**
 * Tests basic module functions.
 *
 * @group symfony_mailer
 */
class BasicEmailTest extends SymfonyMailerKernelTestBase {

  /**
   * Basic email sending test.
   */
  public function testEmail() {
    // Test email error.
    $this->testMailer->verify('zzz');
    $this->readMail();
    $this->assertError('Email "zzz" does not comply with addr-spec of RFC 2822.');

    // Test email success.
    $this->testMailer->verify($this->addressTo);
    $this->readMail();
    $this->assertNoError();
    $this->assertSubject("Verification email from Example");
    $this->assertTo($this->addressTo);
  }

}
