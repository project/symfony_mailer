<?php

namespace Drupal\symfony_mailer_test;

use Drupal\Core\State\StateInterface;
use Drupal\symfony_mailer\EmailInterface;
use Drupal\symfony_mailer\Processor\EmailProcessorInterface;
use Drupal\symfony_mailer\Processor\EmailProcessorTrait;

/**
 * Tracks sent emails for testing.
 */
class MailerTestService implements MailerTestServiceInterface, EmailProcessorInterface {

  use EmailProcessorTrait;

  /**
   * The emails that have been sent.
   *
   * @var \Drupal\symfony_mailer\EmailInterface[]
   */
  protected $emails = [];

  /**
   * Constructs the MailerTestService.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   *
   * @internal
   */
  public function __construct(protected readonly StateInterface $state) {
    if ($existing_emails = $this->state->get(self::STATE_KEY, [])) {
      throw new \Exception(count($existing_emails) . ' emails have not been checked.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getEmails() {
    $emails = $this->emails;
    $this->emails = [];
    return $emails;
  }

  /**
   * Post-render function.
   *
   * @param \Drupal\symfony_mailer\EmailInterface $email
   *   The email.
   */
  public function postRender(EmailInterface $email) {
    $email->setTransport('null://default');
  }

  /**
   * Post-send function.
   *
   * @param \Drupal\symfony_mailer\EmailInterface $email
   *   The email.
   */
  public function postSend(EmailInterface $email) {
    $this->emails[] = $email;
    $this->state->set(self::STATE_KEY, $this->emails);
  }

}
