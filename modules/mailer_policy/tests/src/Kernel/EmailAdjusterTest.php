<?php

namespace Drupal\Tests\mailer_policy\Kernel;

use Drupal\Tests\symfony_mailer\Kernel\SymfonyMailerKernelTestBase;
use Drupal\symfony_mailer\EmailInterface;

/**
 * Tests EmailAdjuster plug-ins.
 *
 * @group symfony_mailer
 */
class EmailAdjusterTest extends SymfonyMailerKernelTestBase {

  /**
   * Inline CSS adjuster test.
   */
  public function testInlineCss() {
    // Test an email including the test library.
    $this->testMailer->addCallback(function (EmailInterface $email) {
      $email->addLibrary('symfony_mailer_test/inline_css_test');
    });
    $this->testMailer->verify($this->addressTo);

    $this->readMail();
    $this->assertNoError();
    // The inline CSS from inline.text-small.css should appear.
    $this->assertBodyContains('<h4 class="text-small" style="padding-top: 3px; padding-bottom: 3px; text-align: center; color: white; background-color: #0678be; font-size: smaller; font-weight: bold;">');
    // The imported CSS from inline.day.css should appear.
    $this->assertBodyContains('<span class="day" style="font-style: italic;">');
  }

}
