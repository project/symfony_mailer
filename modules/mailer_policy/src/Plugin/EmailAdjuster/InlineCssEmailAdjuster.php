<?php

namespace Drupal\mailer_policy\Plugin\EmailAdjuster;

use Drupal\Core\Asset\AssetOptimizerInterface;
use Drupal\Core\Asset\AssetResolverInterface;
use Drupal\Core\Asset\AttachedAssets;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\mailer_policy\Attribute\EmailAdjuster;
use Drupal\mailer_policy\EmailAdjusterBase;
use Drupal\symfony_mailer\AutowireTrait;
use Drupal\symfony_mailer\EmailInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;

/**
 * Defines the Inline CSS Email Adjuster.
 */
#[EmailAdjuster(
  id: "mailer_inline_css",
  label: new TranslatableMarkup("Inline CSS"),
  description: new TranslatableMarkup("Add inline CSS."),
  weight: 900,
)]
class InlineCssEmailAdjuster extends EmailAdjusterBase implements ContainerFactoryPluginInterface {

  use AutowireTrait;

  /**
   * The CSS inliner.
   *
   * @var \TijsVerkoyen\CssToInlineStyles\CssToInlineStyles
   */
  protected $cssInliner;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Asset\AssetResolverInterface $assetResolver
   *   The asset resolver.
   * @param \Drupal\Core\Asset\AssetOptimizerInterface $cssOptimizer
   *   The Drupal CSS optimizer.
   *
   * @internal
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected readonly AssetResolverInterface $assetResolver,
    #[Autowire(service: 'asset.css.optimizer')]
    protected readonly AssetOptimizerInterface $cssOptimizer,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->cssInliner = new CssToInlineStyles();
  }

  /**
   * {@inheritdoc}
   */
  public function postRender(EmailInterface $email) {
    // Only inline CSS if we have an html body.
    if ($html_body = $email->getHtmlBody()) {
      $assets = (new AttachedAssets())->setLibraries($email->getLibraries());
      $css = '';
      foreach ($this->assetResolver->getCssAssets($assets, FALSE) as $asset) {
        if (($asset['type'] == 'file') && $asset['preprocess']) {
          // Optimize to process @import.
          $css .= $this->cssOptimizer->optimize($asset);
        }
        else {
          $css .= file_get_contents($asset['data']);
        }
      }

      if ($css) {
        $email->setHtmlBody($this->cssInliner->convert($html_body, $css));
      }
    }
  }

}
