<?php

namespace Drupal\mailer_policy\Plugin\EmailAdjuster;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\mailer_policy\Attribute\EmailAdjuster;

/**
 * Defines the Reply-to Email Adjuster.
 */
#[EmailAdjuster(
  id: "email_reply_to",
  label: new TranslatableMarkup("Reply-to"),
  description: new TranslatableMarkup("Sets the email reply-to header."),
)]
class ReplyToEmailAdjuster extends AddressAdjusterBase {

  /**
   * The name of the associated header.
   */
  protected const NAME = 'reply-to';

}
