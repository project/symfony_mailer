<?php

namespace Drupal\mailer_policy\Plugin\EmailAdjuster;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\mailer_policy\Attribute\EmailAdjuster;
use Drupal\mailer_policy\EmailAdjusterBase;
use Drupal\mailer_transport\Entity\Transport;
use Drupal\symfony_mailer\EmailInterface;

/**
 * Defines the Mailer transport Email Adjuster.
 */
#[EmailAdjuster(
  id: "email_transport",
  label: new TranslatableMarkup("Mailer transport"),
  description: new TranslatableMarkup("Sets the mailer transport alternative."),
)]
class TransportEmailAdjuster extends EmailAdjusterBase {

  /**
   * {@inheritdoc}
   */
  public function build(EmailInterface $email) {
    $email->setTransport($this->configuration['value']);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $options = [];
    foreach (Transport::loadMultiple() as $id => $transport) {
      $options[$id] = $transport->label();
    }

    $form['value'] = [
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $this->configuration['value'] ?? NULL,
      '#required' => TRUE,
      '#description' => $this->t('Transport.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    if ($transport = Transport::load($this->configuration['value'])) {
      return $transport->label();
    }
    return NULL;
  }

}
