<?php

namespace Drupal\mailer_policy;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\mailer_policy\Entity\MailerPolicy;
use Drupal\symfony_mailer\EmailInterface;
use Drupal\symfony_mailer\Processor\EmailProcessorInterface;
use Drupal\symfony_mailer\Processor\EmailProcessorTrait;

/**
 * Provides the email adjuster plugin manager.
 */
class EmailAdjusterManager extends DefaultPluginManager implements EmailAdjusterManagerInterface, EmailProcessorInterface {

  use EmailProcessorTrait;

  /**
   * Constructs the EmailAdjusterManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   *
   * @internal
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/EmailAdjuster', $namespaces, $module_handler, 'Drupal\mailer_policy\EmailAdjusterInterface', 'Drupal\mailer_policy\Attribute\EmailAdjuster');
    $this->setCacheBackend($cache_backend, 'mailer_policy_definitions');
    $this->alterInfo('mailer_adjuster_info');
  }

  /**
   * {@inheritdoc}
   */
  public function init(EmailInterface $email) {
    $policy_config = MailerPolicy::loadInheritedConfig($email->getTag(), $email->getParams());

    // Add adjusters.
    foreach ($policy_config as $plugin_id => $config) {
      if ($this->hasDefinition($plugin_id)) {
        $email->addProcessor($this->createInstance($plugin_id, $config));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build(EmailInterface $email) {
    $policy_config = MailerPolicy::loadInheritedConfig($email->getTag(), $email->getParams());

    // Update the configuration, which may include translated values.
    foreach ($email->getProcessors() as $processor) {
      if ($processor instanceof EmailAdjusterInterface) {
        $plugin_id = $processor->getId();
        $processor->setConfiguration($policy_config[$plugin_id]);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight(int $phase) {
    return 0;
  }

}
