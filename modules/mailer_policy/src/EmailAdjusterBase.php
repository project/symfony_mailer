<?php

namespace Drupal\mailer_policy;

use Drupal\Core\Form\FormStateInterface;
use Drupal\symfony_mailer\Processor\EmailProcessorBase;

/**
 * Defines the base class for EmailAdjuster plug-ins.
 */
abstract class EmailAdjusterBase extends EmailProcessorBase implements EmailAdjusterInterface {

  /**
   * {@inheritdoc}
   */
  const DEFAULT_WEIGHT = 400;

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return $this->pluginId;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

}
