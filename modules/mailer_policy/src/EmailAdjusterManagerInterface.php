<?php

namespace Drupal\mailer_policy;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Provides the interface for the email adjuster plugin manager.
 */
interface EmailAdjusterManagerInterface extends PluginManagerInterface {

}
