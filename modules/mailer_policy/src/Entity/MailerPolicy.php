<?php

namespace Drupal\mailer_policy\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\mailer_policy\AdjusterPluginCollection;
use Drupal\mailer_policy\EmailAdjusterInterface;
use Drupal\symfony_mailer\Processor\MailerManagerInterface;

/**
 * Defines a Mailer Policy configuration entity class.
 *
 * @ConfigEntityType(
 *   id = "mailer_policy",
 *   label = @Translation("Mailer Policy"),
 *   handlers = {
 *     "list_builder" = "Drupal\mailer_policy\MailerPolicyListBuilder",
 *     "form" = {
 *       "edit" = "Drupal\mailer_policy\Form\PolicyEditForm",
 *       "add" = "Drupal\mailer_policy\Form\PolicyAddForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   admin_permission = "administer mailer",
 *   entity_keys = {
 *     "id" = "id",
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/system/mailer/policy/{mailer_policy}",
 *     "delete-form" = "/admin/config/system/mailer/policy/{mailer_policy}/delete",
 *     "collection" = "/admin/config/system/mailer/policy",
 *   },
 *   config_export = {
 *     "id",
 *     "configuration",
 *   }
 * )
 */
class MailerPolicy extends ConfigEntityBase implements EntityWithPluginCollectionInterface, MailerPolicyInterface {

  use StringTranslationTrait;

  /**
   * The unique ID of the policy record.
   *
   * @var string
   */
  protected $id;

  /**
   * The mailer manager.
   *
   * @var \Drupal\symfony_mailer\Processor\MailerManagerInterface
   */
  protected $MailerManager;

  /**
   * The email adjuster manager.
   *
   * @var \Drupal\symfony_mailer\Processor\EmailAdjusterManagerInterface
   */
  protected $emailAdjusterManager;

  /**
   * The label for an unknown value.
   *
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  protected $labelUnknown;

  /**
   * The label for all values.
   *
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  protected $labelAll;

  /**
   * The tag.
   *
   * @var string
   */
  protected $tag;

  /**
   * The tag label.
   *
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup[]
   */
  protected $tagLabel = [];

  /**
   * The entity id.
   *
   * @var string
   */
  protected $entityId;

  /**
   * The entity.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityInterface
   */
  protected $entity;

  /**
   * The mailer definition.
   *
   * @var array
   */
  protected $mailerDefinition;

  /**
   * Configuration for this policy record.
   *
   * An associative array of email adjuster configuration, keyed by the plug-in
   * ID with value as an array of configured settings.
   *
   * @var array
   */
  protected $configuration = [];

  /**
   * The collection of email adjuster plug-ins configured in this policy.
   *
   * @var \Drupal\Core\Plugin\DefaultLazyPluginCollection
   */
  protected $pluginCollection;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $values, $entity_type) {
    parent::__construct($values, $entity_type);
    $this->emailAdjusterManager = \Drupal::service('plugin.manager.email_adjuster');
  }

  /**
   * Parses the Mailer definition.
   *
   * This isn't done in the constructor for performance reasons. We don't need
   * it when sending an email.
   */
  protected function parse() {
    if ($this->MailerManager) {
      return;
    }
    $this->MailerManager = \Drupal::service(MailerManagerInterface::class);
    $this->labelUnknown = $this->t('Unknown');
    $this->labelAll = $this->t('<b>*All*</b>');

    // The root policy with ID '_' applies to all tags.
    if (!$this->id || ($this->id == '_')) {
      $this->tagLabel[] = $this->labelAll;
      return;
    }

    [$this->tag, $this->entityId] = array_pad(explode('..', $this->id), 2, NULL);
    $this->mailerDefinition = $this->MailerManager->getTagDefinition($this->tag, FALSE);

    if (!$this->mailerDefinition) {
      $this->tagLabel[] = $this->labelUnknown;
      return;
    }

    $this->tagLabel = $this->mailerDefinition['labels'];
    if ($this->mailerDefinition['sub_defs']) {
      $this->tagLabel[] = $this->labelAll;
    }

    // Load the entity.
    if ($this->entityId && $meta_key = $this->mailerDefinition['metadata_key']) {
      $this->entity = $this->entityTypeManager()->getStorage($meta_key)->load($this->entityId);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getTag() {
    $this->parse();
    return $this->tag;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity() {
    $this->parse();
    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getTagLabel(int $skip = 0) {
    $this->parse();
    return Markup::create(implode(' » ', array_slice($this->tagLabel, $skip)));
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityLabel() {
    $this->parse();
    if (empty($this->mailerDefinition['metadata_key'])) {
      return '';
    }
    if ($this->entity) {
      return $this->entity->label();
    }
    return $this->entityId ? $this->labelUnknown : $this->labelAll;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    $this->parse();
    $labels = $this->tagLabel;
    array_push($labels, $this->getEntityLabel());
    return implode(' » ', array_filter($labels));
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration;
    if ($this->pluginCollection) {
      $this->pluginCollection->setConfiguration($configuration);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function adjusters() {
    if (!isset($this->pluginCollection)) {
      $this->pluginCollection = new AdjusterPluginCollection($this->emailAdjusterManager, $this->configuration);
    }
    return $this->pluginCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function adjusterDefinitions() {
    return $this->emailAdjusterManager->getDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary($expanded = FALSE) {
    $summary = [];
    $separator = ', ';

    foreach ($this->adjusters()->sort() as $adjuster) {
      $element = $adjuster->getLabel();
      if ($expanded && ($element_summary = $adjuster->getSummary())) {
        if (strlen($element_summary) > EmailAdjusterInterface::MAX_SUMMARY) {
          $element_summary = substr($element_summary, 0, EmailAdjusterInterface::MAX_SUMMARY) . '…';
        }
        $element .= ": $element_summary";
        $separator = '<br>';
      }
      $summary[] = $element;

    }

    return implode($separator, $summary);
  }

  /**
   * {@inheritdoc}
   */
  public function getCommonAdjusters() {
    $this->parse();
    return $this->mailerDefinition['common_adjusters'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return ['adjusters' => $this->adjusters()];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();
    if ($this->entity) {
      $this->addDependency('config', $this->entity->getConfigDependencyName());
    }
    elseif ($provider = $this->mailerDefinition['provider'] ?? NULL) {
      $this->addDependency('module', $provider);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function sort(ConfigEntityInterface $a, ConfigEntityInterface $b) {
    return strnatcasecmp($a->getTagLabel(), $b->getTagLabel()) ?:
      strnatcasecmp($a->getEntityLabel(), $b->getEntityLabel());
  }

  /**
   * {@inheritdoc}
   */
  public static function sortSpecific(ConfigEntityInterface $a, ConfigEntityInterface $b) {
    return count(explode('.', $b->id())) <=> count(explode('.', $a->id())) ?:
      static::sort($a, $b);
  }

  /**
   * {@inheritdoc}
   */
  public static function loadOrCreate(string $id) {
    return static::load($id) ?? static::create(['id' => $id]);
  }

  /**
   * {@inheritdoc}
   */
  public static function loadInheritedConfig(string $id, array $params = []) {
    $config = [];

    // Find the entity in the params. We force creating the policy because we
    // need the entity even when this specific policy isn't set.
    $entity = NULL;
    $policy = MailerPolicy::loadOrCreate($id);
    $policy->parse();
    if ($meta_key = $policy->mailerDefinition['metadata_key'] ?? NULL) {
      $entity = $params[$meta_key] ?? NULL;
    }

    for ($loop_id = $id; $loop_id; $loop_id = static::parentId($loop_id)) {
      if ($entity && $sub_policy = MailerPolicy::load($loop_id . '..' . $entity->id())) {
        $config += $sub_policy->getConfiguration();
      }

      if ($policy = MailerPolicy::load($loop_id)) {
        $config += $policy->getConfiguration();
      }
    }

    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public static function import($id, array $configuration) {
    $policy = static::loadOrCreate($id);
    $configuration += $policy->getConfiguration();

    $inherited = static::loadInheritedConfig(static::parentId($id));
    foreach (array_keys($configuration) as $key) {
      if (isset($inherited[$key]) && static::identicalArray($configuration[$key], $inherited[$key])) {
        unset($configuration[$key]);
      }
    }

    if ($configuration) {
      $policy->setConfiguration($configuration)->save();
    }
    else {
      $policy->delete();
    }
  }

  /**
   * Returns the parent ID.
   *
   * @param string $id
   *   The initial id.
   *
   * @return string
   *   The parent id.
   */
  protected static function parentId(string $id) {
    if ($id == '_') {
      return NULL;
    }
    $pos = strrpos($id, '.');
    return $pos ? substr($id, 0, $pos) : '_';
  }

  /**
   * Compares two arrays recursively.
   *
   * @param array $a
   *   The first array.
   * @param array $b
   *   The second array.
   *
   * @return bool
   *   TRUE if the arrays are identical.
   */
  protected static function identicalArray(array $a, array $b) {
    if (count($a) != count($b)) {
      return FALSE;
    }

    foreach ($a as $key => $value_a) {
      if (!isset($b[$key])) {
        return FALSE;
      }
      $value_b = $b[$key];
      if (is_array($value_a) && is_array($value_b)) {
        if (!static::identicalArray($value_a, $value_b)) {
          return FALSE;
        }
      }
      elseif ($value_a != $value_b) {
        return FALSE;
      }
    }

    return TRUE;
  }

}
