<?php

namespace Drupal\mailer_transport\Plugin\TransportUI;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\mailer_transport\Attribute\TransportUI;

/**
 * Defines the DSN TransportUI plug-in.
 */
#[TransportUI(
  id: "dsn",
  label: new TranslatableMarkup("DSN"),
  description: new TranslatableMarkup("The DSN transport is a generic fallback and should only be used if there is no specific implementation available."),
)]
class DsnTransportUI extends TransportUIBase {

  const DOCS_URL = 'https://symfony.com/doc/current/mailer.html#transport-setup';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'dsn' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['dsn'] = [
      '#type' => 'textfield',
      '#title' => $this->t('DSN'),
      '#maxlength' => 255,
      '#default_value' => $this->configuration['dsn'],
      '#description' => $this->t('DSN for the Transport, see <a href=":docs">documentation</a>.', [':docs' => static::DOCS_URL]),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $dsn = $form_state->getValue('dsn');
    if (parse_url($dsn, PHP_URL_SCHEME) == 'sendmail') {
      // Don't allow bypassing of the checks done by the Sendmail transport.
      $form_state->setErrorByName('dsn', $this->t('Use the Sendmail transport.'));
    }

    try {
      \Drupal::service('Symfony\Component\Mailer\Transport')->fromString($dsn);
    }
    catch (\Exception $e) {
      $form_state->setErrorByName('dsn', $this->t('Invalid DSN: @message', ['@message' => $e->getMessage()]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['dsn'] = $form_state->getValue('dsn');
  }

  /**
   * {@inheritdoc}
   */
  public function getDsn() {
    return $this->configuration['dsn'];
  }

}
