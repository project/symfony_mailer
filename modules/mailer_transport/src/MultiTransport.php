<?php

namespace Drupal\mailer_transport;

use Drupal\mailer_transport\Entity\Transport;
use Drupal\symfony_mailer\Exception\MissingTransportException;
use Symfony\Component\DependencyInjection\Attribute\AutowireIterator;
use Symfony\Component\Mailer\Envelope;
use Symfony\Component\Mailer\SentMessage;
use Symfony\Component\Mailer\Transport as TransportFactory;
use Symfony\Component\Mailer\Transport\TransportInterface;
use Symfony\Component\Mime\RawMessage;

/**
 * Multi-transport which distributes emails amongst a collection of transports.
 */
class MultiTransport implements TransportInterface {

  /**
   * The transport factory.
   *
   * @var \Symfony\Component\Mailer\Transport
   */
  protected readonly TransportFactory $transportFactory;

  /**
   * Array of created transports, keyed by their ID.
   *
   * @var array<string, TransportInterface>
   */
  protected array $transports = [];

  /**
   * Constructs a new multi-transport.
   *
   * @param Iterable<TransportFactoryInterface> $factories
   *   A list of transport factories.
   *
   * @internal
   */
  public function __construct(
    #[AutowireIterator(tag: 'mailer.transport_factory')]
    iterable $factories,
  ) {
    $this->transportFactory = new TransportFactory($factories);
  }

  /**
   * {@inheritdoc}
   */
  public function send(RawMessage $message, ?Envelope $envelope = NULL): ?SentMessage {
    if ($transport_header = $message->getHeaders()->getHeaderBody('X-Transport')) {
      $message->getHeaders()->remove('X-Transport');
    }
    else {
      $transport_header = '';
    }

    if (empty($this->transports[$transport_header])) {
      if (strpos($transport_header, ':') !== FALSE) {
        // Create from transport DSN string.
        $this->transports[$transport_header] = $this->transportFactory->fromString($transport_header);
      }
      else {
        // Create from transport ID.
        $this->transports[$transport_header] = $this->getTransport($transport_header);
      }
    }

    return $this->transports[$transport_header]->send($message, $envelope);
  }

  /**
   * {@inheritdoc}
   */
  public function __toString(): string {
    return static::class;
  }

  /**
   * Gets the transport from a transport ID.
   *
   * @param string $transport_id
   *   The transport ID, or an empty string for the default transport.
   *
   * @return \Symfony\Component\Mailer\Transport\TransportInterface
   *   The transport.
   */
  protected function getTransport(string $transport_id) {
    $transport_config = $transport_id ? Transport::load($transport_id) : Transport::loadDefault();
    if (!$transport_config) {
      $msg = $transport_id ? "Missing transport $transport_id." : "Missing default transport.";
      throw new MissingTransportException($msg);
    }
    return $this->transportFactory->fromString($transport_config->getDsn());
  }

}
