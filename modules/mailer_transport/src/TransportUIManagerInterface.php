<?php

namespace Drupal\mailer_transport;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * TransportUI plugin manager.
 */
interface TransportUIManagerInterface extends PluginManagerInterface {}
