<?php

namespace Drupal\mailer_override\Plugin\Mailer;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\mailer_override\ImportHelperInterface;
use Drupal\mailer_override\LegacyMailerHelperInterface;
use Drupal\symfony_mailer\EmailInterface;
use Drupal\symfony_mailer\Exception\SkipMailException;
use Drupal\symfony_mailer\Processor\MailerPluginBase;

/**
 * Legacy Mailer plug-in that uses a message array.
 */
class LegacyMailer extends MailerPluginBase implements LegacyMailerInterface {

  /**
   * The plugin_id.
   *
   * @var string
   */
  protected $pluginId;

  /**
   * LegacyMailer constructor.
   *
   * @param \Drupal\mailer_override\LegacyMailerHelperInterface $legacyHelper
   *   The legacy mailer helper.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    protected readonly LegacyMailerHelperInterface $legacyHelper,
    protected readonly ModuleHandlerInterface $moduleHandler,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function send(array $message) {
    $this->pluginId = $message['module'];
    $email = $this->newEmail($message['key'])
      ->setParam('legacy_message', $message)
      ->setParam('__disable_customize__', TRUE);
    $this->pluginId = '';

    // The 'To' header is stored directly in the message.
    // @see \Drupal\Core\Mail\Plugin\Mail\PhpMail::mail()
    if (isset($message['to'])) {
      $email->setTo(\Drupal::service(ImportHelperInterface::class)->parseAddress($message['to']));
    }
    return $email->send();
  }

  /**
   * {@inheritdoc}
   */
  public function build(EmailInterface $email) {
    $message = $email->getParam('legacy_message');
    $message += [
      'subject' => '',
      'body' => [],
      'headers' => [],
    ];

    if ($reply = $message['reply-to']) {
      // Match the non-standard lower-case 't' used by Drupal Core.
      $message['headers']['Reply-to'] = $reply;
    }

    // Build the email by invoking hook_mail() on this module.
    $args = [$message['key'], &$message, $message['params']];
    $this->moduleHandler->invoke($message['module'], 'mail', $args);

    // Invoke hook_mail_alter() to allow all modules to alter the resulting
    // email.
    $this->moduleHandler->alter('mail', $message);

    if (!$message['send']) {
      throw new SkipMailException('Send aborted by hook_mail().');
    }

    // Fill the email from the message array.
    $email->setBody($this->legacyHelper->formatBody($message['body']));
    $this->legacyHelper->emailFromArray($email, $message);
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId() {
    return $this->pluginId;
  }

}
