<?php

namespace Drupal\mailer_override;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Mailer Override configuration override.
 */
class MailerConfigOverride implements ConfigFactoryOverrideInterface {

  /**
   * Whether cache has been built.
   *
   * @var bool
   */
  protected $builtCache = FALSE;

  /**
   * Array of config overrides.
   *
   * As required by ConfigFactoryOverrideInterface::loadOverrides().
   *
   * @var array
   */
  protected $configOverrides = [];

  /**
   * Constructs the MailerConfigOverride object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   */
  public function __construct(protected readonly ModuleHandlerInterface $moduleHandler) {}

  /**
   * {@inheritdoc}
   */
  public function loadOverrides($names) {
    $this->buildCache();
    return array_intersect_key($this->configOverrides, array_flip($names));
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheSuffix() {
    return 'MailerOverride';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($name) {
    return new CacheableMetadata();
  }

  /**
   * {@inheritdoc}
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }

  /**
   * Build cache of config overrides.
   */
  protected function buildCache() {
    if (!$this->builtCache && $this->moduleHandler->isLoaded()) {
      // Getting the definitions can cause reading of config which triggers
      // `loadOverrides()` to call this function. Protect against an infinite
      // loop by marking the cache as built before starting.
      $this->builtCache = TRUE;

      // We cannot use dependency injection because that creates a circular
      // dependency.
      /** @var \Drupal\mailer_override\OverrideManagerInterface $overrideManager */
      $overrideManager = \Drupal::service(OverrideManagerInterface::class);
      foreach ($overrideManager->getDefinitions() as $definition) {
        $this->configOverrides = array_merge($this->configOverrides, $definition['config_overrides']);
      }
    }
  }

}
