<?php

namespace Drupal\mailer_override;

use Drupal\symfony_mailer\EmailInterface;
use Drupal\symfony_mailer\Exception\SkipMailException;
use Drupal\symfony_mailer\Processor\EmailProcessorInterface;
use Drupal\symfony_mailer\Processor\EmailProcessorTrait;

/**
 * EmailProcessor for MailManagerReplacement.
 */
class ReplacementEmailProcessor implements EmailProcessorInterface {

  use EmailProcessorTrait;

  /**
   * CallbackEmailProcessor constructor.
   *
   * @param \Drupal\mailer_override\LegacyMailerHelperInterface $legacyHelper
   *   The legacy mailer helper.
   * @param array $message
   *   The legacy message array.
   *
   * @internal
   */
  public function __construct(protected readonly LegacyMailerHelperInterface $legacyHelper, protected array &$message) {}

  /**
   * {@inheritdoc}
   */
  public function postRender(EmailInterface $email) {
    if (empty($this->message['send'])) {
      throw new SkipMailException('MailManagerInterface call with $send = FALSE');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSend(EmailInterface $email) {
    $this->message['result'] = !$email->getError();
    $this->legacyHelper->emailToArray($email, $this->message);
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight(int $phase) {
    return 1000;
  }

}
