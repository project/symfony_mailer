<?php

namespace Drupal\mailer_override;

use Drupal\Core\Mail\MailManagerInterface;

/**
 * Provides a Symfony Mailer replacement for MailManager.
 */
interface MailManagerReplacementInterface extends MailManagerInterface {}
