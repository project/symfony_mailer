<?php

namespace Drupal\mailer_override;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\symfony_mailer\AutowireTrait;
use Drupal\symfony_mailer\Processor\EmailProcessorInterface;
use Drupal\symfony_mailer\Processor\MailerManagerInterface;
use Drupal\symfony_mailer\Processor\MailerPluginInterface;

/**
 * Defines the Override plug-in for user module.
 */
abstract class OverrideBase extends PluginBase implements OverrideInterface, ContainerFactoryPluginInterface {

  use AutowireTrait;

  /**
   * The mailer.
   */
  protected readonly MailerPluginInterface $mailer;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\symfony_mailer\Processor\MailerManagerInterface $mailerManager
   *   The mailer manager.
   * @param \Drupal\symfony_mailer\LegacyMailerHelperInterface $legacyHelper
   *   The legacy mailer helper.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    MailerManagerInterface $mailerManager,
    protected readonly LegacyMailerHelperInterface $legacyHelper,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->mailer = $mailerManager->createInstance($plugin_id);
  }

  /**
   * {@inheritdoc}
   */
  public function send(array &$message, EmailProcessorInterface $processor) {
    $this->mailer->addProcessor($processor);
    return $this->fromArray($message);
  }

  /**
   * Converts from an legacy message array to send an email.
   *
   * @param array $message
   *   The array to send from.
   *
   * @return bool
   *   Whether successful.
   */
  abstract protected function fromArray(array $message);

}
