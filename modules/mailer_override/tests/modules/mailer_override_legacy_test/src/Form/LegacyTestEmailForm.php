<?php

namespace Drupal\mailer_override_legacy_test\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\symfony_mailer\AutowireTrait;

/**
 * Test module form to send a test legacy email.
 */
class LegacyTestEmailForm extends FormBase {

  use AutowireTrait;

  /**
   * An email 'to' address to use in this form and all tests run.
   */
  const ADDRESS_TO = 'mailer_override-legacy_test-to@example.com';

  /**
   * An email 'cc' address to use in this form and all tests run.
   */
  const ADDRESS_CC = 'mailer_override-legacy_test-cc@example.com';

  /**
   * An email 'bcc' address to use in this form and all tests run.
   */
  const ADDRESS_BCC = 'mailer_override-legacy_test-bcc@example.com';

  /**
   * Constructs TestMailForm.
   *
   * @param \Drupal\Core\Mail\MailManagerInterface $mailManager
   *   The mail manager service.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $themeManager
   *   The theme manager service.
   *
   * @internal
   */
  public function __construct(protected readonly MailManagerInterface $mailManager, protected readonly ThemeManagerInterface $themeManager) {}

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mailer_override_legacy_test_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Send test email',
    ];
    $current_theme = $this->themeManager->getActiveTheme()->getName();
    $form['current_theme'] = [
      '#markup' => 'Current theme: ' . $current_theme,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->mailManager->mail('mailer_override_legacy_test', 'legacy_test', self::ADDRESS_TO, 'en');
  }

}
